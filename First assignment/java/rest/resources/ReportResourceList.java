package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 3/25/2016.
 */
public class ReportResourceList extends ResourceSupport {
    private List<ReportResource> reports= new ArrayList<ReportResource>();

    public List<ReportResource> getReports() {
        return reports;
    }

    public void setReports(List<ReportResource> reports) {
        this.reports = reports;
    }


}