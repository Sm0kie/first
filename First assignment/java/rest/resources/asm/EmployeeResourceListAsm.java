package rest.resources.asm;

import core.models.entities.Employee;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.EmployeeController;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;

import java.util.List;

/**
 * Created by ilyes on 3/18/2016.
 */
public class EmployeeResourceListAsm extends ResourceAssemblerSupport<List<Employee>,EmployeeResourceList> {
    public EmployeeResourceListAsm(){
        super(EmployeeController.class,EmployeeResourceList.class);
    }
    @Override
    public EmployeeResourceList toResource(List<Employee> employeeList) {
            List<EmployeeResource> resList = new EmployeeResourceAsm().toResources(employeeList);
            EmployeeResourceList finalRes = new EmployeeResourceList();
            finalRes.setEmployees(resList);
        return finalRes;
    }
}
