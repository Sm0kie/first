package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 3/23/2016.
 */
public class AdminResourceList extends ResourceSupport {
    private List<AdminResource> admins= new ArrayList<AdminResource>();

    public List<AdminResource> getAdmins() {
        return admins;
    }

    public void setAdmins(List<AdminResource> admins) {
        this.admins = admins;
    }
}
