package rest.controller;

import core.dao.AccountDao;
import core.dao.EmployeeDao;
import core.dao.ReportDao;
import core.models.entities.Account;
import core.models.entities.Employee;
import core.models.entities.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import rest.resources.AccountResource;
import rest.resources.AccountResourceList;
import rest.resources.asm.AccountResourceAsm;
import rest.resources.asm.AccountResourceListAsm;

import java.util.Date;
import java.util.List;

/**
 * Created by ilyes on 3/17/2016.
 */
@RestController
@RequestMapping("/rest/accounts")
public class AccountController {
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private ReportDao reportDao;
    @Autowired
    private EmployeeDao employeeDao;
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResourceList> findAllAccounts() {
        List<Account> accounts = accountDao.findAllAccounts();
        AccountResourceList res =new AccountResourceListAsm().toResource(accounts);
        return new ResponseEntity<AccountResourceList>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResource> findAccountById(@PathVariable Long accountId) {
        Account account = accountDao.findAccountById(accountId);

        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' viewed account "+ accountId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        if (account == null)
            return new ResponseEntity<AccountResource>(HttpStatus.NO_CONTENT);
        AccountResource res = new AccountResourceAsm().toResource(account);
        return new ResponseEntity<AccountResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/{accountId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResource> deleteAccount(@PathVariable Long accountId) {
        Account account = accountDao.deleteAccount(accountId);
        if (account == null)
            return new ResponseEntity<AccountResource>( HttpStatus.NO_CONTENT);
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' deleted account "+ accountId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        AccountResource res = new AccountResourceAsm().toResource(account);
        return new ResponseEntity<AccountResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/{accountId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResource> updateAccount(@PathVariable Long accountId, @RequestBody Account sentAccount) {
        Account updatedAccount = accountDao.updateAccount(accountId, sentAccount);
        if (updatedAccount == null)
            return new ResponseEntity<AccountResource>( HttpStatus.NOT_FOUND);
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' updated account "+ accountId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        AccountResource res = new AccountResourceAsm().toResource(updatedAccount);
        return new ResponseEntity<AccountResource>(res,HttpStatus.OK);
    }
}
