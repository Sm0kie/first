package core.dao;

import core.models.entities.Client;

import java.util.List;

/**
 * Created by ilyes on 3/16/2016.
 */
public interface ClientDao {

    List<Client> findAllClients();
    Client findClientById(Long id);
    List<Client> findClientByName(String firstName, String lastName);
    Client findClientByPNC(String PNC);
    Client createClient(Client data);
    Client updateClient(long clientId, Client data);
    Client removeClient(Long id);
}
