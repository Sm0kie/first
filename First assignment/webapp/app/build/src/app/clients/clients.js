angular.module( 'ngBoilerplate.client', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.home',
  'ngBoilerplate.account'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'manageClient', {
    url: '/clients/client?clientId',
    views: {
      'main': {
        controller: 'ClientManageCtrl',
        templateUrl: 'clients/clients.tpl.html'
      }
    },
    data:{ pageTitle: 'Client' },
    resolve:{
        client: function(clientInfoService,$stateParams){
            return clientInfoService.getClientById($stateParams.clientId);
        },
        account: function(accountInfoService,$stateParams){
            return accountInfoService.getAccountById($stateParams.accountId);
        }
    }
    })
    .state('updateClient',{
     url:'/clients/update/client?clientId',
     views:{
        'main':{
            controller: 'UpdateClientCtrl',
            templateUrl:'clients/update_client.tpl.html'
        }
     },
     data:{ pageTitle: 'Update Client Info'},
     resolve:{
        client: function(clientInfoService,$stateParams){
             return clientInfoService.getClientById($stateParams.clientId);
         }
    }
    });
})
.factory('clientInfoService',function($resource){
     var service={};
     service.getClientById = function(clientId) {
         var Client = $resource("/bank/rest/clients/:paramClientId");
         return Client.get({paramClientId:clientId}).$promise;
     };
     service.removeClient=function(clientId){
        var Client=$resource("/bank/rest/clients/:paramClientId");
        return Client.remove({paramClientId:clientId}).$promise;
     };
     service.updateClientInfo=function(clientId,updatedClient){
        var Client=$resource("/bank/rest/clients/:paramClientId");
        return Client.save({paramClientId:clientId},updatedClient).$promise;
     };
     service.addClient=function(savedClient){
        var Client=$resource("/bank/rest/clients/registerClient");
        return Client.save(savedClient).$promise;
     };
     return service;
 })
 .controller('UpdateClientCtrl',function($scope,$state,clientInfoService,client,sessionService){
     $scope.isLoggedIn = sessionService.isLoggedIn;
     $scope.logout = sessionService.logout;
     $scope.client=client;
     $scope.updateClientInfo=function(firstName,lastName,age,email,personalNumericalCard,identityCardSeries,identityCardNumber,address){
        clientInfoService.updateClientInfo(client.rid,{
            firstName: client.firstName,
            lastName: client.lastName,
            email:client.email,
            age: client.age,
            personalNumericalCard : client.personalNumericalCard,
            identityCardSeries: client.identityCardSeries,
            identityCardNumber: client.identityCardNumber,
            address:client.address
        }).then(function(){
            $state.go("manageClient",{clientId:client.rid},{reload:true});
        });
     };
  })
.controller('ClientManageCtrl',function($scope,$state,clientInfoService,client,accountInfoService,account,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.client=client;
     $scope.removeClient=function(){
             clientInfoService.removeClient(client.rid).then(function(){
                 $state.go("clientSearch",{reload:true});
             });
             };
     $scope.account=account;
     $scope.createAccount=function(clientId,amount,type){
        accountInfoService.createAccount(client.rid,{
                 type: $scope.type,
                 amount: account.amount,
                 owner:account.owner
             }).then(function(){
                 $state.go("accountSearch",{reload:true});
             });
     };
 });