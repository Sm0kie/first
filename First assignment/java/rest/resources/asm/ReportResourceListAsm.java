package rest.resources.asm;

import core.models.entities.Report;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ReportController;
import rest.resources.ReportResource;
import rest.resources.ReportResourceList;

import java.util.List;

/**
 * Created by ilyes on 3/25/2016.
 */
public class ReportResourceListAsm extends ResourceAssemblerSupport<List<Report>,ReportResourceList> {

    public ReportResourceListAsm(){
        super(ReportController.class,ReportResourceList.class);
    }
    @Override
    public ReportResourceList toResource(List<Report> reports) {
        List<ReportResource> resList= new ReportResourceAsm().toResources(reports);
        ReportResourceList finalRes=new ReportResourceList();
        finalRes.setReports(resList);
        return finalRes;
    }
}
