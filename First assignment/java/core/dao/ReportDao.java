package core.dao;

import core.models.entities.Report;

import java.util.List;

/**
 * Created by ilyes on 3/25/2016.
 */
public interface ReportDao {
    List<Report> findAllReports();
    Report createReport(Report data);
}
