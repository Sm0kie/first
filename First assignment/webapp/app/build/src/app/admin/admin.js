angular.module( 'ngBoilerplate.admin', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.employee'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'admin_home', {
    url: '/home/admin?adminId',
    views: {
      'main': {
        controller: 'AdminHomeCtrl',
        templateUrl: 'admin/admin_home.tpl.html'
      }
    },
    resolve:{
        admin: function(adminInfoService, $stateParams) {
                            return adminInfoService.getAdminById($stateParams.adminId);
        }
    },
    data:{ pageTitle: 'Home' }
})
.state( 'employeeSearch', {
      url: '/employee',
      views: {
        'main': {
          controller: 'EmployeeCtrl',
          templateUrl: 'admin/employees.tpl.html'
        }
      },
      data:{ pageTitle: 'Employee' },
       resolve: {
           employees: function(employeeInfoService) {
               return employeeInfoService.getAllEmployees();
           }
       }
   })
.state( 'reportSearch', {
      url: '/reports',
      views: {
        'main': {
          controller: 'ReportCtrl',
          templateUrl: 'admin/reports.tpl.html'
        }
      },
      data:{ pageTitle: 'Reports' },
       resolve: {
           reports: function(reportService) {
               return reportService.getAllReports();
           }
       }
   });
})
.factory('reportService',function($resource){
    var service={};
    service.getAllReports=function(){
            var Reports=$resource("/bank/rest/reports");
            return Reports.get().$promise.then(function(data){
                return data.reports;
            });
        };
    return service;
})
.factory('adminInfoService',function($resource){
    var service={};
    service.getAdminById = function(adminId) {
            var Admin= $resource("/bank/rest/admins/:paramAdminId");
            return Admin.get({paramAdminId:adminId}).$promise;
        };
    return service;
})
.controller('ReportCtrl',function($scope, $state,sessionService,reports){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.reports=reports;
})
.controller('EmployeeCtrl',function($scope, $state,employeeInfoService,sessionService,employees){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.addEmployee=function(firstName,lastName,email,username,password){
             employeeInfoService.addEmployee({
                 firstName: firstName,
                 lastName: lastName,
                 email:email,
                 username:username,
                 password:password
             }).then(function(){
                 $state.go($state.current,{},{reload:true});
             });
         };
    $scope.employees=employees;
})
.controller( 'AdminHomeCtrl', function HomeController( $state,admin,$scope, sessionService,adminService,$resource) {
      $scope.isLoggedIn = sessionService.isLoggedIn;
         $scope.logout = sessionService.logout;
         if(!sessionService.isLoggedIn()){
             sessionService.logout();
             $state.go("login");
         }
     var Admin=$resource("/bank/rest/admins") ;
     var arg=localStorage.getItem("user");
     var data=Admin.get({username:arg.username, password:arg.password}).$promise.then(function(data){
        $state.go("admin_home",{adminId : data.admins[0].rid});
     });

      $scope.admin=admin;
  });