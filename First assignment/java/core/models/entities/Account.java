package core.models.entities;

import javax.persistence.*;

/**
 * Created by ilyes on 3/12/2016.
 */
@Entity
@Table(name="Account")
public class Account {
    @Id
    @GeneratedValue
    @Column(name="id",nullable = false)
    private Long id;
    @Column(name="type",nullable = false)
    private String type;
    @Column(name="amount",nullable=false,precision=3)
    private double amount;

    @ManyToOne
    private Client owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getOwner() {
        return owner;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }
}
