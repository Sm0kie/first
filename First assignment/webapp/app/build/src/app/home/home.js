angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'ngBoilerplate.login',
  'ngBoilerplate.client',
  'ngBoilerplate.account',
  'ngBoilerplate.employee',
  'ngResource'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home/employee?employeeId',
    views: {
      'main': {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    resolve:{
        employee: function(employeeInfoService, $stateParams) {
                            return employeeInfoService.getEmployeeById($stateParams.employeeId);
        }
    },
    data:{ pageTitle: 'Home' }
})
.state('accountSearch',{
        url:'/accounts',
        views:{
            'main':{
                templateUrl:'home/accounts.tpl.html',
                controller:'AccountCtrl'
            }
        },
        data:{pageTitle:'Accounts'},
        resolve:{
            accounts: function(accountService){
                return accountService.getAllAccounts();
            }
        }
})
.state( 'clientSearch', {
      url: '/clients',
      views: {
        'main': {
          controller: 'ClientCtrl',
          templateUrl: 'home/clients.tpl.html'
        }
      },
      data:{ pageTitle: 'Clients' },
       resolve: {
           clients: function(clientService) {
               return clientService.getAllClients();
           }
       }
   });
})
.factory('accountService',function($resource){
    var service={};
    service.getAllAccounts=function(){
        var Account=$resource("/bank/rest/accounts");
        return Account.get().$promise.then(function(data){
            return data.accounts;
        });
    };
    return service;
})
.factory('clientService',function($resource){
    var service={};
    service.getAllClients=function(){
        var Client=$resource("/bank/rest/clients");
        return Client.get().$promise.then(function(data){
            return data.clients;
        });
    };
    return service;
})
.controller('AccountCtrl',function($scope, $state,sessionService,accounts){
$scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.accounts=accounts;
})
.controller('ClientCtrl',function($scope, $state,clientInfoService,sessionService,clients){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.addClient=function(firstName,lastName,age,email,personalNumericalCard,identityCardSeries,identityCardNumber,address){
             clientInfoService.addClient({
                 firstName: firstName,
                 lastName: lastName,
                 email:email,
                 age: age,
                 personalNumericalCard : personalNumericalCard,
                 identityCardSeries: identityCardSeries,
                 identityCardNumber: identityCardNumber,
                 address:address
             }).then(function(){
                 $state.go($state.current,{},{reload:true});
             });
         };
    $scope.clients=clients;
})
.controller( 'HomeCtrl', function HomeController( $state,employee,$scope, sessionService,employeeService,$resource) {
    $scope.isLoggedIn = sessionService.isLoggedIn;
       $scope.logout = sessionService.logout;
       if(!sessionService.isLoggedIn()){
           sessionService.logout();
           $state.go("login");
       }
   var Employee=$resource("/bank/rest/employees") ;
   var arg=localStorage.getItem("user");
   var data=Employee.get({username:arg.username, password:arg.password}).$promise.then(function(data){
      $state.go("home",{employeeId : data.employees[0].rid});
   });

    $scope.employee=employee;
});

