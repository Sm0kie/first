ASSIGNMENT A1
====================================================================
1. Objective
The objective of this assignment is to allow students to become familiar with architectural patterns.

2. Application Description
Use Swing/C# API to design and implement an application for the front desk employees of a bank. The application should have two types of users (a regular user represented by the front desk employee and an administrator user) which have to provide a username and a password in order to use the application.
The regular user can perform the following operations:
- Add/update/view client information (name, identity card number, personal numerical code, address, etc.).
- Create/update/delete/view client account (account information: identification number, type, amount of money, date of creation).
- Transfer money between accounts.
- Process utilities bills.
The administrator user can perform the following operations:
- CRUD on employees’ information.
- Generate reports for a particular period containing the activities performed by an employee.