package rest.resources.asm;

import core.models.entities.Account;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.AccountController;
import rest.resources.AccountResource;
import rest.resources.AccountResourceList;

import java.util.List;

/**
 * Created by ilyes on 3/20/2016.
 */
public class AccountResourceListAsm extends ResourceAssemblerSupport<List<Account>, AccountResourceList> {


    public AccountResourceListAsm() {
        super(AccountController.class, AccountResourceList.class);
    }

    @Override
    public AccountResourceList toResource(List<Account> accountList) {
        List<AccountResource> resList = new AccountResourceAsm().toResources(accountList);
        AccountResourceList finalRes = new AccountResourceList();
        finalRes.setAccounts(resList);
        return finalRes;
    }
}
