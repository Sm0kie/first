package rest.resources;

import core.models.entities.Client;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 3/17/2016.
 */
public class ClientResource extends ResourceSupport {
    private long rid;
    private String firstName;
    private String lastName;
    private String email;
    private int age;
    private String personalNumericalCard;
    private String identityCardNumber;
    private String identityCardSeries;
    private String address;

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPersonalNumericalCard() {
        return personalNumericalCard;
    }

    public void setPersonalNumericalCard(String personalNumericalCard) {
        this.personalNumericalCard = personalNumericalCard;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getIdentityCardSeries() {
        return identityCardSeries;
    }

    public void setIdentityCardSeries(String identityCardSeries) {
        this.identityCardSeries = identityCardSeries;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Client toClient(){
        Client client = new Client();
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setEmail(email);
        client.setAddress(address);
        client.setAge(age);
        client.setIdentityCardNumber(identityCardNumber);
        client.setIdentityCardSeries(identityCardSeries);
        client.setPersonalNumericalCard(personalNumericalCard);
        return client;
    }
}
