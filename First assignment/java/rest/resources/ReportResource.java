package rest.resources;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 3/25/2016.
 */
public class ReportResource extends ResourceSupport {
    private Long rid;
    private String report;
    private String date;
    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
