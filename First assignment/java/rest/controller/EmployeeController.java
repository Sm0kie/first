package rest.controller;

import core.dao.AccountDao;
import core.dao.ClientDao;
import core.dao.EmployeeDao;
import core.dao.ReportDao;
import core.models.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.AccountResource;
import rest.resources.ClientResource;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;
import rest.resources.asm.AccountResourceAsm;
import rest.resources.asm.ClientResourceAsm;
import rest.resources.asm.EmployeeResourceAsm;
import rest.resources.asm.EmployeeResourceListAsm;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 3/17/2016.
 */
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private ReportDao reportDao;

    @RequestMapping(value = "/rest/employees", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResourceList> findAllEmployees(@RequestParam(value = "username", required = false) String username, @RequestParam(value = "password", required = false) String password) {
        List<Employee> employees = null;
        if (username == null) {
            employees = employeeDao.findAllEmployees();
        } else {
            Employee employee = employeeDao.findEmployeeByUserName(username);
            employees = new ArrayList<Employee>();
            if (employee != null) {
                if (password != null) {
                    if (employee.getPassword().equals(password)) {
                        employees = new ArrayList<Employee>(Arrays.asList(employee));
                    }
                } else {
                    employees = new ArrayList<Employee>(Arrays.asList(employee));
                }
            }

        }
        EmployeeResourceList empl = new EmployeeResourceListAsm().toResource(employees);
        return new ResponseEntity<EmployeeResourceList>(empl, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/employees/{employeeId}", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<Employee> findEmployeeById(@PathVariable Long employeeId) {
        Employee employee = employeeDao.findEmployeeById(employeeId);
        if (employee == null)
            return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/employees/{employeeId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> removeEmployee(@PathVariable Long employeeId) {
        Employee employee = employeeDao.removeEmployee(employeeId);
        if (employee != null)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NO_CONTENT);
        EmployeeResource res = new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res, HttpStatus.OK);

    }

    @RequestMapping(value = "/rest/employees/{employeeId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> updateEmployee(@PathVariable Long employeeId, @RequestBody Employee sentAccount) {
        Employee updatedEmployee = employeeDao.updateEmployee(employeeId, sentAccount);
        if (updatedEmployee == null)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NO_CONTENT);
        EmployeeResource res = new EmployeeResourceAsm().toResource(updatedEmployee);
        return new ResponseEntity<EmployeeResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/clients/registerClient", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResource> createClientAccount(@RequestBody Client sentClient, UriComponentsBuilder ucBuilder) {
        Client createdClient = clientDao.createClient(sentClient);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rest/clients/{clientId}").buildAndExpand(sentClient.getId()).toUri());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Employee employee = employeeDao.findEmployeeByUserName(authentication.getName());
        Report report = new Report();
        report.setReport("Employee with username '" + employee.getUsername() + "' created new client account");
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        ClientResource res = new ClientResourceAsm().toResource(createdClient);
        return new ResponseEntity<ClientResource>(res, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/clients/{clientId}/accounts", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResource> createAccount(@PathVariable Long clientId, @RequestBody Account sentAccount, UriComponentsBuilder ucBuilder) {
        Account createdAccount = accountDao.createAccount(sentAccount);
        createdAccount.setOwner(clientDao.findClientById(clientId));
        accountDao.updateAccount(createdAccount.getId(), createdAccount);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Employee employee = employeeDao.findEmployeeByUserName(authentication.getName());
        Report report = new Report();
        report.setReport("Employee with username '" + employee.getUsername() + "' created new account" + clientId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rest/clients/{clientId}/accounts/{accountId}").buildAndExpand(clientId, createdAccount.getId()).toUri());
        AccountResource res = new AccountResourceAsm().toResource(createdAccount);
        return new ResponseEntity<AccountResource>(res, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/bill", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Void> processUtilityBills(@RequestBody Bills bill) {
        Account account = accountDao.findAccountById(bill.getAccountId());
        if (account.getAmount() - bill.getAmount() < 0)
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        account.setAmount(account.getAmount() - bill.getAmount());
        accountDao.updateAccount(account.getId(), account);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Employee employee = employeeDao.findEmployeeByUserName(authentication.getName());
        Report report = new Report();
        report.setReport("Employee with username '" + employee.getUsername() + "' requested a utility bill processing with ID: " + bill.getBillId() + " and amount: " + bill.getAmount());
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/transfer", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> transferBetweenAccounts(@RequestBody Transfer transfer) {
        Account account1 = accountDao.findAccountById(transfer.getAccountId1());
        Account account2 = accountDao.findAccountById(transfer.getAccountId2());
        if (account1.getAmount() - transfer.getAmount() < 0)
            return new ResponseEntity<String>("Insufficient funds", HttpStatus.CONFLICT);
        account1.setAmount(account1.getAmount() - transfer.getAmount());
        account2.setAmount(account2.getAmount() + transfer.getAmount());
        accountDao.updateAccount(account1.getId(), account1);
        accountDao.updateAccount(account2.getId(), account2);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Employee employee = employeeDao.findEmployeeByUserName(authentication.getName());
        Report report = new Report();
        report.setReport("Employee with username '" + employee.getUsername() + "' requested a transfer between accounts " + transfer.getAccountId1() + " and " + transfer.getAccountId2() + " with sum " + transfer.getAmount());
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        return new ResponseEntity<String>("Transaction successful", HttpStatus.OK);
    }
}
