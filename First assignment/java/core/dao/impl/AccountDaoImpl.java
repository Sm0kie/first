package core.dao.impl;

import core.dao.AccountDao;
import core.models.entities.Account;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 3/16/2016.
 */
@Service
@Transactional
public class AccountDaoImpl implements AccountDao {


    @PersistenceContext
    private EntityManager em;

    @Override
    public Account findAccountById(Long id) {
        return em.find(Account.class,id);
    }

    @Override
    public List<Account> findAllAccounts() {
        Query query=em.createQuery("SELECT a from Account a");
        return query.getResultList();
    }

    @Override
    public List<Account> findAccountsByOwner(Long ownerId) {
        Query query= em.createQuery("SELECT b from Account b where b.owner.id=?1");
        query.setParameter(1,ownerId);
        return query.getResultList();
    }

    @Override
    public Account createAccount(Account data) {
        if(data.getType()==null)
        data.setType("Spending");
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }
    @Override
    public Account updateAccount(Long accountId,Account data){
        Account account=em.find(Account.class,accountId);
        account.setAmount(data.getAmount());
        account.setType(data.getType());
        account.setOwner(data.getOwner());
        em.merge(account);
        return account;
    }

    @Override
    public Account deleteAccount(Long id) {
        Account deletedAccount= em.find(Account.class,id);
        em.remove(deletedAccount);
        return deletedAccount;
    }
}
