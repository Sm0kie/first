package rest.resources.asm;

import core.models.entities.Client;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ClientController;
import rest.resources.ClientResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 3/20/2016.
 */
public class ClientResourceAsm extends ResourceAssemblerSupport<Client,ClientResource> {

public ClientResourceAsm()
        {
        super(ClientController.class, ClientResource.class);
        }

@Override
public ClientResource toResource(Client client) {
        ClientResource res = new ClientResource();
        res.setRid(client.getId());
        res.setFirstName(client.getFirstName());
        res.setLastName(client.getLastName());
        res.setIdentityCardNumber(client.getIdentityCardNumber());
        res.setIdentityCardSeries(client.getIdentityCardSeries());
        res.setEmail(client.getEmail());
        res.setPersonalNumericalCard(client.getPersonalNumericalCard());
        res.setAddress(client.getAddress());
        res.setAge(client.getAge());

        res.add(linkTo(methodOn(ClientController.class).findClientById(client.getId())).withSelfRel());
        return res;
        }
}
