package core.models.entities;

import javax.persistence.*;

/**
 * Created by ilyes on 3/12/2016.
 */
@Entity
@Table(name="Client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="firstName",length=10)
    private String firstName;
    @Column(name="lastName",length=10)
    private String lastName;
    @Column(name="email",length=50)
    private String email;
    @Column(name="age")
    private int age;
    @Column(name="CNP",length=13,unique = true)
    private String personalNumericalCard;
    @Column(name="IDN",length=6)
    private String identityCardNumber;
    @Column(name="IDS",length=2)
    private String identityCardSeries;
    @Column(name="address")
    private String address;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPersonalNumericalCard() {
        return personalNumericalCard;
    }

    public void setPersonalNumericalCard(String personalNumericalCard) {
        this.personalNumericalCard = personalNumericalCard;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getIdentityCardSeries() {
        return identityCardSeries;
    }

    public void setIdentityCardSeries(String identityCardSeries) {
        this.identityCardSeries = identityCardSeries;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
