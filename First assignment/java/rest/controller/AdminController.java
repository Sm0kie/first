package rest.controller;

import core.dao.AdminDao;
import core.dao.EmployeeDao;
import core.dao.ReportDao;
import core.models.entities.Admin;
import core.models.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.AdminResourceList;
import rest.resources.asm.AdminResourceListAsm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 3/18/2016.
 */
@RestController
public class AdminController {
    @Autowired
    private AdminDao adminDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private ReportDao reportDao;
    @RequestMapping(value="/rest/admins",method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<AdminResourceList> findAllAdmins(@RequestParam(value="username",required=false) String username, @RequestParam(value="password",required=false) String password) {
        List<Admin> admins =null;
        if(username==null){
            admins=adminDao.findAllAdmins();
        }else{
            Admin admin=adminDao.findAdminByUserName(username);
            admins=new ArrayList<>();
            if(admin!=null){
                if(password!=null){
                    if(admin.getPassword().equals(password)){
                        admins=new ArrayList<Admin>(Arrays.asList(admin));
                    }
                }else{
                    admins=new ArrayList<Admin>(Arrays.asList(admin));
                }
            }
        }
        AdminResourceList adm=new AdminResourceListAsm().toResource(admins);
        return new ResponseEntity<AdminResourceList>(adm, HttpStatus.OK);
    }
    @RequestMapping(value = "/rest/employees/createEmployee", method = RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee sentAccount, UriComponentsBuilder ucBuilder) {
        Employee employee = employeeDao.createEmployee(sentAccount);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rest/employees/{employeeId}").buildAndExpand(employee.getId()).toUri());
        return new ResponseEntity<Employee>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/admins/{adminId}", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<Admin> findAdminById(@PathVariable Long adminId) {
        Admin admin = adminDao.findAdminById(adminId);
        if (admin == null)
            return new ResponseEntity<Admin>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<Admin>(admin, HttpStatus.OK);
    }


}
