package core.dao.impl;

import core.dao.ReportDao;
import core.models.entities.Report;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 3/25/2016.
 */
@Service
@Transactional
public class ReportDaoImpl implements ReportDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Report> findAllReports() {
        Query query=em.createQuery("SELECT a FROM Report a");
        return query.getResultList();
    }

    @Override
    public Report createReport(Report data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }
}
