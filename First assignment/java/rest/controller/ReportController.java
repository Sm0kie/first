package rest.controller;

import core.dao.ReportDao;
import core.models.entities.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rest.resources.ReportResourceList;
import rest.resources.asm.ReportResourceListAsm;

import java.util.List;

/**
 * Created by ilyes on 3/25/2016.
 */
@RestController
public class ReportController {
    @Autowired
    private ReportDao reportDao;
    @RequestMapping(value="/rest/reports",method= RequestMethod.GET)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<ReportResourceList> findAllReports(){
        List<Report> reports=reportDao.findAllReports();
        if (reports.isEmpty())
            return new ResponseEntity<ReportResourceList>(HttpStatus.NO_CONTENT);
        ReportResourceList res=new ReportResourceListAsm().toResource(reports);
        return new ResponseEntity<ReportResourceList>(res, HttpStatus.OK);

    }
}
