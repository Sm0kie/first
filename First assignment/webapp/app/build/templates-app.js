angular.module('templates-app', ['account/account.tpl.html', 'account/update_account.tpl.html', 'admin/admin_home.tpl.html', 'admin/employees.tpl.html', 'admin/reports.tpl.html', 'clients/clients.tpl.html', 'clients/update_client.tpl.html', 'employee/employee.tpl.html', 'employee/update_employee.tpl.html', 'home/accounts.tpl.html', 'home/clients.tpl.html', 'home/home.tpl.html', 'login/login.tpl.html']);

angular.module("account/account.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/account.tpl.html",
    "<html ng-app=\"ngBoilerplate.account\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div ng-control=\"AccountManageCtrl\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Account\n" +
    "        </h1>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{account.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Amount: </th>\n" +
    "            <td>{{account.amount}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Owner: </th>\n" +
    "            <td>{{account.owner.personalNumericalCard}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Type: </th>\n" +
    "            <td>{{account.type}}</td>\n" +
    "                </span>\n" +
    "        </p>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeAccount({accountId:account.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateAccount({accountId:account.rid})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Transfer\n" +
    "        </h1>\n" +
    "        <label>Amount:</label>\n" +
    "        <input type=\"text\" ng-model=\"amount\" min=\"0\"  pattern=\"[0-9]*.[0.9]{3}\" class=\"form-control\" required/>\n" +
    "        <label>Account id:</label>\n" +
    "        <input type=\"text\" ng-model=\"accountId2\" min=\"0\" class=\"form-control\" required/>\n" +
    "        <input type=\"submit\" value=\"Transfer\" ng-click=\"transferBetweenAccounts(accountId2,amount)\" class=\"btn btn-default\"/>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Process utility bill\n" +
    "        </h1>\n" +
    "        <label>Amount:</label>\n" +
    "        <input type=\"text\" ng-model=\"amount\" min=\"0\"  pattern=\"[0-9]*.[0.9]{3}\" class=\"form-control\" required/>\n" +
    "        <label>Bill id:</label>\n" +
    "        <input type=\"text\" ng-model=\"billId\" min=\"0\" minlength=\"5\" maxlength=\"5\" class=\"form-control\" required/>\n" +
    "        <input type=\"submit\" value=\"Process\" ng-click=\"processUtility(billId,amount)\" class=\"btn btn-default\"/>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("account/update_account.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/update_account.tpl.html",
    "<html ng-app=\"ngBoilerplate.account\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<body>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Account Information\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"UpdateAccountCtrl\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Amount:</label>\n" +
    "        <input type=\"text\" ng-model=\"account.amount\" class=\"form-control\" required/>\n" +
    "        <label>Type:</label>\n" +
    "        <form name=\"myForm\" >\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"type\" value=\"Spending\"required/>\n" +
    "                Spending\n" +
    "            </label>\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"type\" value=\"Savings\"required/>\n" +
    "                Savings\n" +
    "            </label>\n" +
    "        </form>\n" +
    "\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateAccountInfo({amount:account.amount},{type:type})\" class=\"btn btn-warning\"/>\n" +
    "</div>\n" +
    "</body>\n" +
    "</html>");
}]);

angular.module("admin/admin_home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/admin_home.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"AdminHomeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"reportSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        View Reports\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Profile\n" +
    "    </h1>\n" +
    "    <div ng-control=\"AdminHomeCtrl\">\n" +
    "        <h1 >\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{admin.id}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{admin.firstName}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{admin.lastName}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Email: </th>\n" +
    "            <td>{{admin.email}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Username:</th>\n" +
    "            <td>{{admin.username}}</td>\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("admin/employees.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/employees.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"reportSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        View Reports\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Add new employee\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"lastName\" class=\"form-control\"required/>\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" ng-model=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" class=\"form-control\"required/>\n" +
    "        <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addEmployee(firstName,lastName,email,username,password)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Employees\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"Employee Info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>First Name</th>\n" +
    "        <th>LastName</th>\n" +
    "        <th>Email</th>\n" +
    "        <th>Username</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"employee in employees |filter:search\">\n" +
    "            <td>{{employee.rid}}</td>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            <td>{{employee.email}}</td>\n" +
    "            <td>{{employee.username}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageEmployee({employeeId:employee.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("admin/reports.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/reports.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"ReportCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"reportSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        View Reports\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Reports\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\" placeholder=\"Employee Info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>Activity</th>\n" +
    "        <th>Date</th>\n" +
    "        <tr ng-repeat=\"report in reports |filter:search\">\n" +
    "            <td>{{report.rid}}</td>\n" +
    "            <td>{{report.report}}</td>\n" +
    "            <td>{{report.date}}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("clients/clients.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("clients/clients.tpl.html",
    "<html ng-app=\"ngBoilerplate.client\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Client\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div ng-control=\"ClientManageCtrl\">\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{client.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{client.firstName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{client.lastName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Age: </th>\n" +
    "            <td>{{client.age}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>Email: </th>\n" +
    "            <td>{{client.email}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>CNP: </th>\n" +
    "                    <td>{{client.personalNumericalCard}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>IDS: </th>\n" +
    "                    <td>{{client.identityCardSeries}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "            <th>IDN: </th>\n" +
    "            <td>{{client.identityCardNumber}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>Address: </th>\n" +
    "            <td>{{client.address}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeClient({clientId:client.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateClient({clientId:client.rid})\" class=\"btn btn-default\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Create Account\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Amount:</label>\n" +
    "        <input type=\"text\" ng-model=\"account.amount\" min=\"0\" class=\"form-control\" required/>\n" +
    "        <label>Type:</label>\n" +
    "        <form name=\"myForm\" >\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"type\" value=\"Spending\"required/>\n" +
    "                Spending\n" +
    "            </label>\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"type\" value=\"Savings\"required/>\n" +
    "                Savings\n" +
    "            </label>\n" +
    "        </form>\n" +
    "\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Create\" ng-click=\"createAccount({clientId:client.rid},{amount:account.amount},{type:type})\" class=\"btn btn-default\"/>\n" +
    "</div></div>\n" +
    "</html>");
}]);

angular.module("clients/update_client.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("clients/update_client.tpl.html",
    "<html ng-app=\"ngBoilerplate.client\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<body>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Client Information\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"ClientManageCtrl\">\n" +
    "<div class=\"form-group\"  >\n" +
    "    <label>First name:</label>\n" +
    "    <input type=\"text\" ng-model=\"client.firstName\" class=\"form-control\" required/>\n" +
    "    <label>Last name:</label>\n" +
    "    <input type=\"text\" ng-model=\"client.lastName\" class=\"form-control\"required/>\n" +
    "    <label>Age:</label>\n" +
    "    <input type=\"number\" ng-model=\"client.age\" min=\"18\" max=\"99\" class=\"form-control\"required/>\n" +
    "    <label>Email:</label>\n" +
    "    <input type=\"email\" ng-model=\"client.email\" class=\"form-control\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" required/>\n" +
    "    <label>CNP:</label>\n" +
    "    <input type=\"text\" minlength=\"13\" maxlength=\"13\" pattern=\"[1-6]{1}+[0-9]{12}\" title=\"The CNP must be 13 characters.\" ng-model=\"client.personalNumericalCard\" class=\"form-control\"required/>\n" +
    "    <label>IDS:</label>\n" +
    "    <input type=\"text\" minlength=\"2\" maxlength=\"2\" pattern=\"[A-Z]{2}\" title=\"The cards identification must be 13 characters.\" ng-model=\"client.identityCardSeries\" class=\"form-control\"required/>\n" +
    "    <label>IDN:</label>\n" +
    "    <input type=\"text\" minlength=\"6\" maxlength=\"6\" pattern=\"[0-9]{6}\" title=\"The CNP must be 13 characters.\" ng-model=\"client.identityCardNumber\" class=\"form-control\"required/>\n" +
    "    <label>Address:</label>\n" +
    "    <input type=\"text\" ng-model=\"client.address\" class=\"form-control\"required/>\n" +
    "\n" +
    "</div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateClientInfo({firstName: client.firstName},{lastName: client.lastName},{age: client.age},{email: client.email},{personalNumericalCard:client.personalNumericalCard},{identityCardSeries:client.identityCardSeries},{identityCardNumber:client.identityCardNumber},{address:client.address})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "<div>\n" +
    "\n" +
    "</div>\n" +
    "</body>\n" +
    "</html>");
}]);

angular.module("employee/employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.employee\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{employee.id}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Email: </th>\n" +
    "            <td>{{employee.email}}</td>\n" +
    "                </span>\n" +
    "        </p>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeEmployee({employeeId:employee.id})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateEmployee({employeeId:employee.id})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("employee/update_employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/update_employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  >\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"UpdateEmployeeCtrl\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Update Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.lastName\" class=\"form-control\"required/>\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" ng-model=\"employee.email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" class=\"form-control\"required/>\n" +
    "        <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"employee.password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateEmployeeInfo({firstName: employee.firstName},{lastName: employee.lastName},{email: employee.email},{username:employee.username},{password:employee.password})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/accounts.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/accounts.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\" ng-controller=\"AccountCtrl\" >\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Accounts\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"q\" placeholder=\"account id\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>Account Id</th>\n" +
    "        <th>Amount</th>\n" +
    "        <th>Account Owner</th>\n" +
    "        <th>Account Type</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"account in accounts|filter:q \">\n" +
    "            <td>{{account.rid}}</td>\n" +
    "            <td>{{account.amount}}</td>\n" +
    "            <td>{{account.owner.personalNumericalCard}}</td>\n" +
    "            <td>{{account.type}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageAccount({accountId:account.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/clients.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/clients.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\" ng-controller=\"ClientCtrl\" >\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Add new client\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"lastName\" class=\"form-control\"required/>\n" +
    "        <label>Age:</label>\n" +
    "        <input type=\"number\" ng-model=\"age\" min=\"18\" max=\"99\" class=\"form-control\"required/>\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" ng-model=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" class=\"form-control\"required/>\n" +
    "        <label>CNP:</label>\n" +
    "        <input type=\"text\" minlength=\"13\" maxlength=\"13\" pattern=\"[1-6]{1]+[0-9]{12}\" title=\"The CNP must be 13 characters.\" ng-model=\"personalNumericalCard\" class=\"form-control\"required/>\n" +
    "        <label>IDS:</label>\n" +
    "        <input type=\"text\" minlength=\"2\" maxlength=\"2\" pattern=\"[A-Z]{2}$\" title=\"The cards identification must be 13 characters.\" ng-model=\"identityCardSeries\" class=\"form-control\"required/>\n" +
    "        <label>IDN:</label>\n" +
    "        <input type=\"text\" minlength=\"6\" maxlength=\"6\" pattern=\"[0-9]{6}$\" title=\"The CNP must be 13 characters.\" ng-model=\"identityCardNumber\" class=\"form-control\"required/>\n" +
    "        <label>Address:</label>\n" +
    "        <input type=\"text\" ng-model=\"address\" class=\"form-control\"required/>\n" +
    "\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addClient(firstName,lastName,age,email,personalNumericalCard,identityCardSeries,identityCardNumber,address)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Clients\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"client name\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>First Name</th>\n" +
    "        <th>LastName</th>\n" +
    "        <th>Email</th>\n" +
    "        <th>CNP</th>\n" +
    "        <th>IDS</th>\n" +
    "        <th>IDN</th>\n" +
    "        <th>Age</th>\n" +
    "        <th>Address</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"client in clients |filter:search\">\n" +
    "            <td>{{client.rid}}</td>\n" +
    "            <td>{{client.firstName}}</td>\n" +
    "            <td>{{client.lastName}}</td>\n" +
    "            <td>{{client.email}}</td>\n" +
    "            <td>{{client.personalNumericalCard}}</td>\n" +
    "            <td>{{client.identityCardSeries}}</td>\n" +
    "            <td>{{client.identityCardNumber}}</td>\n" +
    "            <td>{{client.age}}</td>\n" +
    "            <td>{{client.address}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageClient({clientId:client.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\"  ng-controller=\"HomeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"clientSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"accountSearch\">\n" +
    "                        <i class=\"fa fa-book\"></i>\n" +
    "                        Search Accounts\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Profile\n" +
    "    </h1>\n" +
    "    <div ng-control=\"HomeCtrl\">\n" +
    "        <h1 >\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{employee.id}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Email: </th>\n" +
    "            <td>{{employee.email}}</td>\n" +
    "        </h1>\n" +
    "        <h1>\n" +
    "            <th>Username:</th>\n" +
    "            <td>{{employee.username}}</td>\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/login.tpl.html",
    "<html ng-app=\"ngBoilerplate.login\" ng-controller=\"LoginCtrl\">\n" +
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "      Login\n" +
    "  </h1>\n" +
    "  <form ng-submit=\"login()\">\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Username:</label>\n" +
    "          <input type=\"text\" ng-model=\"user.username\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Password:</label>\n" +
    "          <input type=\"password\" ng-model=\"user.password\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <button class=\"btn btn-success\" type=\"submit\">Login</button>\n" +
    "  </form>\n" +
    "</div>\n" +
    "\n" +
    "</html>");
}]);
