package core.security;

import core.dao.AdminDao;
import core.models.entities.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by ilyes on 3/24/2016.
 */
@Component
public class AdminDetailServiceImpl implements UserDetailsService {

    @Autowired
    private AdminDao service;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
       Admin admin = service.findAdminByUserName(name);
        if(admin == null) {
            throw new UsernameNotFoundException("no user found with " + name);
        }
        return new AdminDetails(admin);
    }
}
