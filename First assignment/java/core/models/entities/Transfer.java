package core.models.entities;

/**
 * Created by ilyes on 3/25/2016.
 */
public class Transfer {
    private Long accountId1;
    private Long accountId2;
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Long getAccountId2() {
        return accountId2;
    }

    public void setAccountId2(Long accountId2) {
        this.accountId2 = accountId2;
    }

    public Long getAccountId1() {
        return accountId1;
    }

    public void setAccountId1(Long accountId1) {
        this.accountId1 = accountId1;
    }
}
