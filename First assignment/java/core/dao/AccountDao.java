package core.dao;

import core.models.entities.Account;

import java.util.List;

/**
 * Created by ilyes on 3/16/2016.
 */
public interface AccountDao  {
    Account findAccountById(Long id);
    List<Account> findAllAccounts();
    List<Account> findAccountsByOwner(Long ownerId);
    Account createAccount(Account data);
    Account updateAccount(Long accountId, Account data);
    Account deleteAccount(Long id);
}
