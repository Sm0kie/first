package rest.resources.asm;

import core.models.entities.Account;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.AccountController;
import rest.resources.AccountResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 3/20/2016.
 */
public class AccountResourceAsm extends ResourceAssemblerSupport<Account,AccountResource>{
    public AccountResourceAsm() {
        super(AccountController.class, AccountResource.class);
    }
    @Override
    public AccountResource toResource(Account account) {
        AccountResource res = new AccountResource();
        res.setAmount(account.getAmount());
        res.setRid(account.getId());
        res.setType(account.getType());
        res.setOwner(account.getOwner());
        res.add(linkTo(methodOn(AccountController.class).findAccountById(account.getId())).withSelfRel());
        if(account.getOwner()!=null)
            res.add(linkTo(AccountController.class).slash(account.getOwner().getId()).withRel("owner"));
        return res;
    }
}
