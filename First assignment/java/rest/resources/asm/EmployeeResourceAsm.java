package rest.resources.asm;

import core.models.entities.Employee;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.EmployeeController;
import rest.resources.EmployeeResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
/**
 * Created by ilyes on 3/18/2016.
 */
public class EmployeeResourceAsm extends ResourceAssemblerSupport<Employee,EmployeeResource> {

    public EmployeeResourceAsm()
    {
        super(EmployeeController.class, EmployeeResource.class);
    }

    @Override
    public EmployeeResource toResource(Employee employee) {
        EmployeeResource res = new EmployeeResource();
        res.setRid(employee.getId());
        res.setFirstName(employee.getFirstName());
        res.setLastName(employee.getLastName());
        res.setUsername(employee.getUsername());
        res.setPassword(employee.getPassword());
        res.setEmail(employee.getEmail());
        res.add(linkTo(methodOn(EmployeeController.class).findEmployeeById(employee.getId())).withSelfRel());
        return res;
    }
}
