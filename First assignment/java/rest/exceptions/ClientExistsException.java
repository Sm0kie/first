package rest.exceptions;

/**
 * Created by ilyes on 3/23/2016.
 */
public class ClientExistsException extends RuntimeException {
    public ClientExistsException() {
    }

    public ClientExistsException(String message) {
        super(message);
    }

    public ClientExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientExistsException(Throwable cause) {
        super(cause);
    }
}