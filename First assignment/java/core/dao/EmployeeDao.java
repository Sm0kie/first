package core.dao;

import core.models.entities.Employee;

import java.util.List;

/**
 * Created by ilyes on 3/20/2016.
 */
public interface EmployeeDao {
    List<Employee> findAllEmployees();

    Employee findEmployeeById(Long id);

    Employee findEmployeeByUserName(String username);

    Employee createEmployee(Employee data);

    Employee updateEmployee(Long EmployeeId, Employee data);

    Employee removeEmployee(Long id);
}
