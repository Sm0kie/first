package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 3/17/2016.
 */
public class ClientResourceList extends ResourceSupport{
    private List<ClientResource> clients = new ArrayList<ClientResource>();

    public List<ClientResource> getClients() {
        return clients;
    }

    public void setClients(List<ClientResource> clients) {
        this.clients = clients;
    }
}
