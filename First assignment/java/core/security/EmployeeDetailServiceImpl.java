package core.security;

import core.dao.EmployeeDao;
import core.models.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by Chris on 10/19/14.
 */
@Component
public class EmployeeDetailServiceImpl implements UserDetailsService {

    @Autowired
    private EmployeeDao service;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Employee employee = service.findEmployeeByUserName(name);
        if(employee == null) {
            throw new UsernameNotFoundException("no user found with " + name);
        }
        return new EmployeeDetails(employee);
    }
}
