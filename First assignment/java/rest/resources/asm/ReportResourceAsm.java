package rest.resources.asm;

import core.models.entities.Report;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ReportController;
import rest.resources.ReportResource;

/**
 * Created by ilyes on 3/25/2016.
 */
public class ReportResourceAsm extends ResourceAssemblerSupport<Report,ReportResource> {

    public ReportResourceAsm(){
        super(ReportController.class,ReportResource.class);
    }
    @Override
    public ReportResource toResource(Report report) {
        ReportResource res = new ReportResource();
        res.setRid(report.getId());
        res.setReport(report.getReport());
        res.setDate(report.getDate());
        return res;
    }
}
