package rest.resources.asm;

import core.models.entities.Client;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ClientController;
import rest.resources.ClientResource;
import rest.resources.ClientResourceList;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 3/20/2016.
 */
public class ClientResourceListAsm extends ResourceAssemblerSupport<List<Client>, ClientResourceList> {
    public ClientResourceListAsm() {
        super(ClientController.class, ClientResourceList.class);
    }

    @Override
    public ClientResourceList toResource(List<Client> list) {
        List<ClientResource> resources = new ClientResourceAsm().toResources(list);
        ClientResourceList listResource = new ClientResourceList();
        listResource.setClients(resources);
        listResource.add(linkTo(methodOn(ClientController.class).findAllClients()).withSelfRel());
        return listResource;
    }
}