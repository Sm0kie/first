package core.dao.impl;

import core.dao.ClientDao;
import core.models.entities.Client;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 3/16/2016.
 */
@Service
@Transactional
public class ClientDaoImpl implements ClientDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Client> findAllClients() {
        Query query=em.createQuery("SELECT a FROM Client a");
        return query.getResultList();
    }

    @Override
    public Client findClientById(Long id) {
        return em.find(Client.class,id);
    }

    @Override
    public List<Client> findClientByName(String firstName, String lastName) {
        Query query=em.createQuery("SELECT b from Client where b.firstname=?1 AND b.lastname=?2");
        query.setParameter(1,firstName);
        query.setParameter(2,lastName);
        return query.getResultList();
    }

    @Override
    public Client findClientByPNC(String PNC) {
        Query query=em.createQuery("SELECT b from Client where b.PNC=?1");
        query.setParameter(1,PNC);
        List<Client> clients=query.getResultList();
        if(clients.isEmpty())
            return null;
        else
            return clients.get(0);
    }

    @Override
    public Client createClient(Client data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }
    @Override
    public Client updateClient(long clientId,Client data)
     {
         Client client=em.find(Client.class,clientId);
         client.setEmail(data.getEmail());
         client.setFirstName(data.getFirstName());
         client.setLastName(data.getLastName());
         client.setPersonalNumericalCard(data.getPersonalNumericalCard());
         client.setAddress(data.getAddress());
         client.setAge(data.getAge());
         client.setIdentityCardSeries(data.getIdentityCardSeries());
         client.setIdentityCardNumber(data.getIdentityCardNumber());
         em.merge(client);
         return client;
    }

    @Override
    public Client removeClient(Long id) {
        Client client =em.find(Client.class,id);
        em.remove(client);
        return client;
    }
}
