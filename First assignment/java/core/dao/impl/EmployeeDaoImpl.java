package core.dao.impl;

import core.dao.EmployeeDao;
import core.models.entities.Employee;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 3/16/2016.
 */
@Service
@Transactional
public class EmployeeDaoImpl implements EmployeeDao {

    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Employee> findAllEmployees() {
        Query query=em.createQuery("SELECT a FROM Employee a");
        return query.getResultList();
    }

    @Override
    public Employee findEmployeeById(Long id) {
        return em.find(Employee.class,id);
    }

/*    @Override
    public List<Employee> findEmployeeByName(String firstName, String lastName) {
        Query query=em.createQuery("SELECT b from Employee WHERE b.firstname=?1 AND b.lastname=?2");
        query.setParameter(1,firstName);
        query.setParameter(2,lastName);
        return query.getResultList();
    }
*/
    @Override
    public Employee findEmployeeByUserName(String username) {
        Query query=em.createQuery("SELECT a from Employee a WHERE a.username=?1");
        query.setParameter(1,username);
        List<Employee> employees=query.getResultList();
        if(employees.isEmpty())
            return null;
        else
            return employees.get(0);
    }

    @Override
    public Employee createEmployee(Employee data){
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }

    @Override
    public Employee updateEmployee(Long employeeId,Employee data) {
        Employee employee = em.find(Employee.class,employeeId);
        employee.setPassword(data.getPassword());
        employee.setUsername(data.getUsername());
        employee.setLastName(data.getLastName());
        employee.setFirstName(data.getFirstName());
        employee.setEmail(data.getEmail());
        return employee;
    }

    @Override
    public Employee removeEmployee(Long id) {
        Employee employee=em.find(Employee.class,id);
        em.remove(employee);
        return employee;
    }
}
