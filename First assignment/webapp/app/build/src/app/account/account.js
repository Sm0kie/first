angular.module( 'ngBoilerplate.account', [
  'ui.router',
  'plusOne',
  'base64',
  'ngBoilerplate.login',
  'ngBoilerplate.home',
  'ngBoilerplate.client'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'manageAccount', {
    url: '/accounts/account?accountId',
    views: {
      'main': {
        controller: 'AccountManageCtrl',
        templateUrl: 'account/account.tpl.html'
      }
    },
    data:{ pageTitle: 'Account' },
    resolve:{
        account: function(accountInfoService,$stateParams){
            return accountInfoService.getAccountById($stateParams.accountId);
        }
    }
    })
    .state('updateAccount',{
         url:'/accounts/account/update/account?accountId',
         views:{
            'main':{
                controller: 'UpdateAccountCtrl',
                templateUrl:'account/update_account.tpl.html'
            }
         },
         data:{ pageTitle: 'Update Account Info'},
         resolve:{
            account: function(accountInfoService,$stateParams){
                 return accountInfoService.getAccountById($stateParams.accountId);
             }
        }
    });
})
.factory('accountInfoService',function($resource,$http,$base64){
     var service={};
     service.getAccountById = function(accountId) {
         var Account = $resource("/bank/rest/accounts/:paramAccountId");
         return Account.get({paramAccountId:accountId}).$promise;
     };
     service.removeAccount=function(accountId){
        var Account=$resource("/bank/rest/accounts/:paramAccountId");
        return Account.remove({paramAccountId:accountId}).$promise;
     };
     service.updateAccountInfo=function(accountId,updatedAccount){
        var Account=$resource("/bank/rest/accounts/:paramAccountId");
        return Account.save({paramAccountId:accountId},updatedAccount).$promise;
     };
     service.addAccount=function(savedAccount){
        var Account=$resource("/bank/rest/accounts/registerAccount");
        return Account.save(savedAccount).$promise;
     };
      service.createAccount=function(clientId,savedClient){
         var Client=$resource("/bank/rest/clients/:paramAccountId/accounts");
         return Client.save({paramAccountId:clientId},savedClient).$promise;
      };
      service.transferBetweenAccounts= function(transfer) {
           var Transfer=$resource("/bank/rest/transfer");
           return Transfer.save(transfer).$promise;
      };
      service.processBillUtility= function(bill) {
           var Transfer=$resource("/bank/rest/bill");
           return Transfer.save(bill).$promise;
      };
     return service;
 })
 .controller('UpdateAccountCtrl',function($scope,$state,accountInfoService,account,sessionService){
      $scope.isLoggedIn = sessionService.isLoggedIn;
      $scope.logout = sessionService.logout;
      $scope.account=account;

      $scope.updateAccountInfo=function(amount,type){
         accountInfoService.updateAccountInfo(account.rid,{
             type: $scope.type,
             amount: account.amount,
             owner:account.owner
         }).then(function(){
             $state.go("manageAccount",{accountId:account.rid},{reload:true});
         });
      };
   })
.controller('AccountManageCtrl',function($scope,$state,accountInfoService,account,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.account=account;
     $scope.removeAccount=function(){
             accountInfoService.removeAccount(account.rid).then(function(){
                 $state.go("accountSearch",{reload:true});
             });
             };
     $scope.transferBetweenAccounts=function(accountId2,amount){
        accountInfoService.transferBetweenAccounts({
            accountId1:account.rid,
            accountId2: $scope.accountId2,
            amount: $scope.amount
        }).then(function(){
         $state.go("accountSearch",{reload:true});
        });
     };
     $scope.processUtility=function(billId,amount){
        accountInfoService.processBillUtility({
        accountId: account.rid,
        billId: $scope.billId,
        amount: $scope.amount
        }).then(function(){
            $state.go("accountSearch",{reload:true});
        });
     };
 });