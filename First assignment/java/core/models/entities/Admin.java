package core.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by ilyes on 3/20/2016.
 */
@Entity
public class Admin extends User {
    @Column(name="superuser")
    private boolean superUser;
    @Column(name="username")
    private String username;
    @Column(name="password")
    private String password;


    public boolean getSuperUser() {
        return superUser;
    }


    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
