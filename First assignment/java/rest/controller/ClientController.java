package rest.controller;

import core.dao.AccountDao;
import core.dao.ClientDao;
import core.dao.EmployeeDao;
import core.dao.ReportDao;
import core.models.entities.Account;
import core.models.entities.Client;
import core.models.entities.Employee;
import core.models.entities.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.AccountResourceList;
import rest.resources.ClientResource;
import rest.resources.ClientResourceList;
import rest.resources.asm.AccountResourceListAsm;
import rest.resources.asm.ClientResourceAsm;
import rest.resources.asm.ClientResourceListAsm;

import java.util.Date;
import java.util.List;

/**
 * Created by ilyes on 3/17/2016.
 */
@RestController
@RequestMapping("/rest/clients")
public class ClientController {
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private ReportDao reportDao;
    @Autowired
    private EmployeeDao employeeDao;
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResourceList> findAllClients() {
        List<Client> clients = clientDao.findAllClients();
        if (clients.isEmpty())
            return new ResponseEntity<ClientResourceList>(HttpStatus.NO_CONTENT);
        ClientResourceList res = new ClientResourceListAsm().toResource(clients);
        return new ResponseEntity<ClientResourceList>(res, HttpStatus.OK);
    }



    @RequestMapping(value = "/{clientId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResource> findClientById(@PathVariable Long clientId) {
        Client client = clientDao.findClientById(clientId);
        if (client == null)
            return new ResponseEntity<ClientResource>( HttpStatus.NOT_FOUND);
        ClientResource res = new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/exists", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResource> findClientByPNC(@PathVariable String CNP) {
        Client client = clientDao.findClientByPNC(CNP);

        if (client == null)
            return new ResponseEntity<ClientResource>( HttpStatus.NOT_FOUND);
        ClientResource res = new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResource> removeClient(@PathVariable Long clientId) {
        Client client = clientDao.removeClient(clientId);
        if (client == null)
            return new ResponseEntity<ClientResource>( HttpStatus.NOT_FOUND);
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' removed client "+ clientId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        ClientResource res = new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ClientResource> updateClient(
            @PathVariable Long clientId, @RequestBody Client sentClient) {
        Client updatedClient = clientDao.updateClient(clientId, sentClient);
        if (updatedClient == null)
            return new ResponseEntity<ClientResource>(HttpStatus.NOT_FOUND);
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' updated client "+ clientId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        ClientResource res = new ClientResourceAsm().toResource(updatedClient);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }


    @RequestMapping(value = "/{clientId}/accounts/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<AccountResourceList> findAccountsOfClient(
            @PathVariable Long clientId,
            UriComponentsBuilder ucBuilder) {
        List<Account> accounts = accountDao.findAccountsByOwner(clientId);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rest/clients/{clientId}/accounts").buildAndExpand(clientId).toUri());
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        Employee employee=employeeDao.findEmployeeByUserName(authentication.getName());
        Report report=new Report();
        report.setReport("Employee with username '"+employee.getUsername()+"' viewed accounts of "+ clientId);
        report.setDate(new Date().toString());
        reportDao.createReport(report);
        AccountResourceList res=new AccountResourceListAsm().toResource(accounts);
        return new ResponseEntity<AccountResourceList>(res, headers, HttpStatus.CREATED);
    }
}
