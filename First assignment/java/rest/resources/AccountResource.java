package rest.resources;

import core.models.entities.Account;
import core.models.entities.Client;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 3/17/2016.
 */
public class AccountResource extends ResourceSupport{
        private Long rid;
        private String type;
        private double amount;
        private Client owner;

        public Long getRid() {
            return rid;
        }

        public void setRid(long id) {
            this.rid = id;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Account toAccount(){
            Account account = new Account();
            account.setId(rid);
            account.setAmount(amount);
            account.setType(type);
            account.setOwner(owner);
            return account;
        }

    public Client getOwner() {
        return owner;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }
}
