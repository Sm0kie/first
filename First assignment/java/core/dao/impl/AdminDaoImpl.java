package core.dao.impl;

import core.dao.AdminDao;
import core.models.entities.Admin;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 3/18/2016.
 */
@Service
@Transactional
public class AdminDaoImpl implements AdminDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Admin> findAllAdmins() {
        Query query=em.createQuery("SELECT a FROM Admin a");
        return query.getResultList();
    }
    @Override
    public Admin findAdminByUserName(String username) {
        Query query=em.createQuery("SELECT a from Admin a WHERE a.username=?1");
        query.setParameter(1,username);
        List<Admin> admin=query.getResultList();
        if(admin.isEmpty())
            return null;
        else
            return admin.get(0);
    }

    @Override
    public Admin findAdminById(Long id) {
        return em.find(Admin.class,id);
    }
}
